package com.example.us.abdiel.Model;

/**
 * Created by mac on 15/04/18.
 */

public class Employ {

    public String id;
    public String name;
    public String phone;
    public String email;
    public String companyEmployID;
    public String address;
    public  boolean hasEmployDetails;
   // public Customer [] myAllCustomers;

public  Employ()
{

    this.id = "";
    this.name = "";
    this.email = "";
    this.phone = "";
    this.companyEmployID = "";
    this.address = "";
    this.hasEmployDetails = false;
}

public Employ(String id , String name, String phone,String email,String companyEmployID, String address)
{
    this.id = id;
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.companyEmployID = companyEmployID;
    this.address = address;
    this.hasEmployDetails = true;

}


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyEmployID() {
        return companyEmployID;
    }

    public void setCompanyEmployID(String companyEmployID) {
        this.companyEmployID = companyEmployID;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
