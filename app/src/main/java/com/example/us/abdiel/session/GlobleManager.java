package com.example.us.abdiel.session;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by mac on 08/04/18.
 */

public class GlobleManager {


    enum SessionType
    {
        NONE, EMPLOY, USER;
    }


    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "globle_manager_pref";

    // All Shared Preferences Keys
    private static final String LOGIN_SESSION_TYPE = "logging_session_type";


    public GlobleManager(Context _context) {
        this.context = _context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void setCurrentSessionType(SessionType sessionType)
    {
        switch (sessionType)
        {

            case NONE:
                editor.putInt(LOGIN_SESSION_TYPE,0);
                break;

            case USER:
                editor.putInt(LOGIN_SESSION_TYPE,1);
                break;

                case EMPLOY:
             editor.putInt(LOGIN_SESSION_TYPE,2);
                break;
        }


        // commit changes
        editor.commit();

    }

    public SessionType getCurrentSessionType()
    {

        SessionType sessionType = SessionType.NONE;;

        int session = pref.getInt(LOGIN_SESSION_TYPE, 0);

        switch (session)
        {
            case 0:
                 sessionType = SessionType.NONE;
            break;

            case 1:

                sessionType = SessionType.USER;
                break;

            case 2:

                sessionType = SessionType.EMPLOY;
                break;

        }

        return sessionType;
    }


    public void saveEmail(String email) {
        //editor.putString(LOGIN_STATUS, email);
        // commit changes
        editor.commit();
    }

}
