package com.example.us.abdiel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.example.us.abdiel.Helper.AlertDialogHelper;
import com.example.us.abdiel.Helper.NetworkHelper;
import com.example.us.abdiel.Model.Employ;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmployCompleteProfile extends AppCompatActivity {


    @BindView(R.id.etEmpFullName)
    EditText etEmpFullName;

    @BindView(R.id.etEmpPhone)
    EditText etEmpPhoneNumber;

    @BindView(R.id.etEmpID)
    EditText etEmpID;
    @BindView(R.id.etEmpAddress)
    EditText etEmpAddress;

    DatabaseReference databaseRef;

    Employ employ;
    FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employ_complete_profile);

      ButterKnife.bind(this);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        currentUser = mAuth.getCurrentUser();

        databaseRef = FirebaseDatabase.getInstance().getReference("employ");

        databaseRef.child(currentUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.w("Dashboard", "didRecicveData", null);
                //
               // employ = null;
                employ = dataSnapshot.getValue(Employ.class);

                AlertDialogHelper.showAlert(EmployCompleteProfile.this, "User Details Added "+employ.getName());
             }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("Dashboard", "No User Data Found", databaseError.toException());
                // ...
            }
        });


    }

    public void OnSubmit(View view)
    {

     if (validation())
     {

         if (NetworkHelper.isNetworkAvaialble(this))
         {
             employ = new Employ(currentUser.getUid(),etEmpFullName.getText().toString(),etEmpPhoneNumber.getText().toString(),currentUser.getEmail(),etEmpID.getText().toString(),etEmpAddress.getText().toString());


             databaseRef.child(currentUser.getUid()).setValue(employ);

         }
         else
         {
             AlertDialogHelper.showAlert(this,"Please check your internet connection");
         }


     }


    }

    public boolean validation()
    {

        if (etEmpFullName.getText().toString().length()==0)
        {
            AlertDialogHelper.showAlert(this,"Please enter your name");
            return false;
        }

        if (etEmpPhoneNumber.getText().toString().length()==0)
        {
            AlertDialogHelper.showAlert(this,"Please enter phone number");
            return false;
        }

        if (etEmpID.getText().toString().length()==0)
        {
            AlertDialogHelper.showAlert(this,"Please enter your employ id");
            return false;
        }

        if (etEmpAddress.getText().toString().length()==0)
        {
            AlertDialogHelper.showAlert(this,"Please enter your address");
            return false;
        }

        return  true;
    }


    public void logoutAction(View view)
    {
        FirebaseAuth.getInstance().signOut();
        Intent i = new Intent(EmployCompleteProfile.this,MainActivity.class);
        startActivity(i);
        finish();
    }
}
