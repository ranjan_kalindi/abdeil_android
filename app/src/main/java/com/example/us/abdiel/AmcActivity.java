package com.example.us.abdiel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AmcActivity extends AppCompatActivity {

    private Button button;
    private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amc);

        button = (Button) findViewById(R.id.button);
        button2 = (Button) findViewById(R.id.button2);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEmployeeActivity();

                button2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                     openCustomerActivitity();
                    }
                });



            }
            public void openCustomerActivitity(){
                Intent abc = new Intent(AmcActivity.this,CustomerActivity.class);
                startActivity(abc);
            }
        });
    }
    public void openEmployeeActivity() {
        Intent intent = new Intent (this,EmployeeActivity.class);
        startActivity(intent);
    }
}
